<?php
require_once "bootstrap.php";

$pais = $argv[1];
$marca = $argv[2];

$fabrica = new Fabrica();
$fabrica->setPais($pais);
$fabrica->setMarca($marca);

$entityManager->persist($fabrica);
$entityManager->flush();

echo "Fabrica criada com o id " . $fabrica->getId() . "\n";
