<?php
use Doctrine\Common\Collections\ArrayCollection;

class Fabrica {

    protected $id;
    protected $codigo;
    protected $pais;
    protected $marca;
    protected $modelos;

    /**
     * @param mixed 
     */
    public function __construct() {
        $this->modelos = new ArrayCollection();
    }

    /**
     * Getter for id
     *
     * return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter for codigo
     *
     * return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    /**
     * Getter for pais
     *
     * return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Getter for marca
     *
     * return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Setter for pais
     *
     * @param string $pais                                                                                                                               
     * @return Fabrica
     */
    public function setPais($pais)
    {
        $this->pais = $pais;
    
        return $this;
    }

    /**
     * Setter for marca
     *
     * @param string $marca                                                                                                                               
     * @return Fabrica
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    
        return $this;
    }
    
    public function addModelo($modelo) {
        $this->modelos[] = $modelo;
    }    
}
