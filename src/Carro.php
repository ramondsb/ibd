<?php
use Doctrine\Common\Collections\ArrayCollection;

class Carro {

    protected $id;
    protected $chassi;
    protected $placa;
    protected $cor;
    protected $alocacoes;

    /**
     * @param mixed 
     */
    public function __construct()
    {
        $this->alocacoes = new ArrayCollection();
    }
    
    /**
     * Getter for id
     *
     * return string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Getter for chassi
     *
     * return string
     */
    public function getChassi()
    {
        return $this->chassi;
    }

    /**
     * Getter for placa
     *
     * return string
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * Getter for cor
     *
     * return string
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * Setter for placa
     *
     * @param string $placa                                                                                                                               
     * @return Carro
     */
    public function setPlaca($placa)
    {
        $this->placa = $placa;
    
        return $this;
    }
    
    /**
     * Setter for cor
     *
     * @param string $cor                                                                                                                               
     * @return Carro
     */
    public function setCor($cor)
    {
        $this->cor = $cor;
    
        return $this;
    }

    /**
     * Setter for chassi
     *
     * @param string $chassi                                                                                                                               
     * @return Carro
     */
    public function setChassi($chassi)
    {
        $this->chassi = $chassi;
    
        return $this;
    }
    

    /**
     * undocumented function
     *
     * @return void
     */
    public function addAlocacao($alocacao)
    {
        return $this->alocacoes[] = $alocacao;
    }
    
    
}
