<?php

class Alocacao {

    protected $id;
    protected $data;
    protected $usuario;
    protected $carro;

    /**
     * Getter for id
     *
     * return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter for data
     *
     * return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Setter for data
     *
     * @param string $data                                                                                                                               
     * @return Alocacao
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }
    
    /**
     * Getter for carro
     *
     * return string
     */
    public function getCarro()
    {
        return $this->carro;
    }

    /**
     * Getter for usuario
     *
     * return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Setter for carro
     *
     * @param string $carro                                                                                                                               
     * @return Alocacao
     */
    public function setCarro($carro)
    {
        $this->carro = $carro;
    
        return $this;
    }
    
    /**
     * Setter for usuario
     *
     * @param string $usuario                                                                                                                               
     * @return Alocacao
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    
        return $this;
    }
    
}
