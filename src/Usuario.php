<?php
use Doctrine\Common\Collections\ArrayCollection;

class Usuario {

    protected $id;
    protected $rg;
    protected $nome;
    protected $cnh;
    protected $nascimento;
    protected $endereco;
    protected $alocacoes;

    /**
     * @param mixed 
     */
    public function __construct() {
        $this->alocacoes = new ArrayCollection();
    }

    /**
     * Getter for id
     *
     * return string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Getter for rg
     *
     * return string
     */
    public function getRg()
    {
        return $this->rg;
    } 

    /**
     * Getter for nome
     *
     * return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Getter for cnh
     *
     * return string
     */
    public function getCnh()
    {
        return $this->cnh;
    }

    /**
     * Getter for nascimento
     *
     * return string
     */
    public function getNascimento()
    {
        return $this->nascimento;
    }

    /**
     * Getter for endereco
     *
     * return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Setter for nome
     *
     * @param string $nome                                                                                                                               
     * @return User
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    
        return $this;
    }
    
    /**
     * Setter for cnh
     *
     * @param string $cnh                                                                                                                               
     * @return User
     */
    public function setCnh($cnh)
    {
        $this->cnh = $cnh;
    
        return $this;
    }
    
    /**
     * Setter for nascimento
     *
     * @param string $nascimento                                                                                                                               
     * @return User
     */
    public function setNascimento($nascimento)
    {
        $this->nascimento = $nascimento;
    
        return $this;
    }
    
    /**
     * Setter for endereco
     *
     * @param string $endereco                                                                                                                               
     * @return User
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    
        return $this;
    }
    
    /**
     * undocumented function
     *
     * @return void
     */
    public function addAlocacao($alocacao)
    {
        return $this->alocacoes[] = $alocacao;
    }

    /**
     * Setter for rg
     *
     * @param string $rg                                                                                                                               
     * @return Usuario
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    
        return $this;
    }
    
}
