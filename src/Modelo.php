<?php

use Doctrine\Common\Collections\ArrayCollection;

class Modelo {

    protected $id;
    protected $codigo;
    protected $nome;
    protected $ano;
    protected $fabrica;

    /**
     * Getter for id
     *
     * return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter for codigo
     *
     * return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Setter for nome
     *
     * @param string $nome                                                                                                                               
     * @return Modelo
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    
        return $this;
    }
    
    /**
     * Getter for ano
     *
     * return string
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * Setter for ano
     *
     * @param string $ano                                                                                                                               
     * @return Modelo
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    
        return $this;
    }
    
    /**
     * Getter for fabrica
     *
     * return string
     */
    public function getFabrica()
    {
        return $this->fabrica;
    }

    /**
     * Setter for fabrica
     *
     * @param string $fabrica                                                                                                                               
     * @return Modelo
     */
    public function setFabrica($fabrica)
    {
        $fabrica->addModelo($this);
        $this->fabrica = $fabrica;

        return $this;
    }
    
    /**
     * Getter for nome
     *
     * return string
     */
    public function getNome()
    {
        return $this->nome;
    }
}
