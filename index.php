<?php
include 'resposta.php';
require 'vendor/autoload.php';

$app = new \Slim\Slim();

$app->get('/', function() use ($app){
    $app->render('index.php');
});

$app->get('/questao/:numero', function($numero) use ($app){
    $app->render('form_'. $numero . '.php');
});

$app->post('/resposta/:questao', function($questao) use ($app){
    echo 'Respondendo a questao ' . $questao . "</br>";
    $resposta = new Resposta();

    $req = $app->request();

    switch($questao) {
    case '1':
        $fabrica = $req->post('fabrica');
        $resposta->q1($fabrica);
        break;
    default:
        echo 'Resposta não encontrada';
    }
});
$app->run();


