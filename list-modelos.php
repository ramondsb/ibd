<?php
require_once "bootstrap.php";

$dql = "SELECT m, f FROM Modelo m JOIN m.fabrica f ORDER BY m.ano DESC";

$query = $entityManager->createQuery($dql);
$query->setMaxResults(30);
$modelos = $query->getResult();

foreach ($modelos as $modelo) {
    echo $modelo->getNome()." - ".$modelo->getAno() . "\n";
    echo "    Fabricado por : ". $modelo->getFabrica()->getMarca()."\n";
    echo "\n";
}
