<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;


class Resposta {

    private $entityManager;
    /**
     * @param mixed 
     */
    public function __construct()
    {
        $isDevMode = true;
        $config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

        // database configuration parameters
        $conn = array(
            'driver' => 'pdo_pgsql',
            'user' => 'postgres',
            'port' => 5432,
            'dbname' => 'ibd'
        );

        // obtaining the entity manager
        $this->entityManager = EntityManager::create($conn, $config);
    }

    public function q1($fabrica) {

        $dql = "SELECT m, f FROM Modelo m JOIN m.fabrica f WHERE f.marca = '" . $fabrica . "' ORDER BY m.ano DESC";

        $query = $this->entityManager->createQuery($dql);
        $query->setMaxResults(30);
        $modelos = $query->getResult();

        echo '<table border="1" style="width:100%">';
        echo '<tr>';
             echo '<td>Modelo</td>';
             echo '<td>Ano</td>';
             echo '<td>Marca</td>';
        echo '</tr>';
        foreach ($modelos as $modelo) {
            echo '<tr>';
                echo '<td>' . $modelo->getNome() . '</td>';
                echo '<td>' . $modelo->getAno() . '</td>';
                echo '<td>' . $modelo->getFabrica()->getMarca() . '</td>';
            echo '</tr>';
        }
        echo '</table>';
      }
}
