<?php
require_once "bootstrap.php";

$chassi = $argv[1];
$placa = $argv[2];
$cor = $argv[3];

$carro = new Carro();
$carro->setChassi($chassi);
$carro->setPlaca($placa);
$carro->setCor($cor);

$entityManager->persist($carro);
$entityManager->flush();

echo "Carro criado com o id " . $carro->getId() . "\n";
