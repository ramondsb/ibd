<?php
require_once "bootstrap.php";

$rg = $argv[1];
$nome = $argv[2];
$cnh = $argv[3];
$endereco = $argv[4];

$usuario = new Usuario();
$usuario->setRg($rg);
$usuario->setNome($nome);
$usuario->setCnh($cnh);
$usuario->setNascimento(new DateTime('12-01-2016'));
$usuario->setEndereco($endereco);

$entityManager->persist($usuario);
$entityManager->flush();

echo "Carro criado com o id " . $usuario->getId() . "\n";
