<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('GMT');
}

$isDevMode = true;
$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

// database configuration parameters
$conn = array(
    'driver' => 'pdo_pgsql',
    'user' => 'postgres',
    'port' => 5432,
    'dbname' => 'ibd'
);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
