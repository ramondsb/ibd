<?php
require_once "bootstrap.php";

$fabricaId = $argv[1];
$nome = $argv[2];
$ano = $argv[3];

$fabrica = $entityManager->find("Fabrica", $fabricaId);
if (!$fabrica) {
    echo "Fabrica não existe\n";
    exit(1);
}

$modelo = new Modelo();
$modelo->setNome($nome);
$modelo->setAno($ano);
$modelo->setFabrica($fabrica);

$entityManager->persist($modelo);
$entityManager->flush();

echo "Modelo criado com o id: ". $modelo->getId() ."\n";
